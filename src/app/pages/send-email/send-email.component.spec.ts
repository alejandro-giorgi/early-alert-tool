import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HeaderSearchComponent } from '../../shared/header-search/header-search.component';
import { HeaderOrgSearchComponent } from '../../shared/header-org-search/header-org-search.component';
import { LeftNavigationComponent } from '../../shared/left-navigation/left-navigation.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import { TextboxComponent } from '../../components/textbox/textbox.component';
import { SendMailFormComponent } from '../../components/send-mail-form/send-mail-form.component';
import { DatePickerComponent } from '../../components/date-picker/date-picker.component';
import { TimePickerComponent } from '../../components/time-picker/time-picker.component';
import { ModelComponent } from '../../components/model/model.component';
import { CheckboxComponent } from '../../components/checkbox/checkbox.component';
import { InputTagComponent } from '../../components/input-tag/input-tag.component';
import { TagInputModule } from 'ngx-chips';
import { QuillModule } from 'ngx-quill';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nConfig, L10nLoader, LocalizationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { LoginComponent } from '../login/login.component';

import { SendEmailComponent } from './send-email.component';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'fr', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

describe('SendEmailComponent', () => {
  let component: SendEmailComponent;
  let fixture: ComponentFixture<SendEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendEmailComponent, HeaderComponent,
                      HeaderSearchComponent, HeaderOrgSearchComponent,
                      LeftNavigationComponent, FooterComponent,
                      SendMailFormComponent, ModelComponent,
                      DropdownComponent, TextboxComponent, CheckboxComponent,
                      InputTagComponent, DatePickerComponent,
                      TimePickerComponent, LoginComponent ],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent}
        ]),
        LocalizationModule.forRoot(l10nConfig),
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        TagInputModule,
        QuillModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
