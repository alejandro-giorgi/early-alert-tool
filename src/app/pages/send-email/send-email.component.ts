import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.scss']
})
export class SendEmailComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  isOpenModal: any;
  modalType = '';
  constructor(private router: Router) { }

  ngOnInit() {
    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    }
  }

  // send email
  sending($event) {
    this.isOpenModal = true;
    this.modalType = $event;
  }
}
