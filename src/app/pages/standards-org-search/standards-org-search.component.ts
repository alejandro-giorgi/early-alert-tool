import { Component, OnInit } from '@angular/core';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-standards-org-search',
  templateUrl: './standards-org-search.component.html',
  styleUrls: ['./standards-org-search.component.scss']
})
export class StandardsOrgSearchComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  divisions: any;
  activeStandard: any;
  tabs = [{
    name: 'Standard 1',
    activeStandard: undefined,
  }];
  activeTab = 0;
  constructor(
    private keywordSearchService: KeywordSearchService
  ) { }

  ngOnInit() {
  }

  selectDivision(std: any) {
    this.tabs[this.activeTab].activeStandard = std;
    this.tabs[this.activeTab].name = std.name;
  }

  getTab(tabs: any) {
    this.tabs = [...tabs];
  }

  getActive(activetab: number) {
    this.activeTab = activetab;
  }
}
