import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HeaderSearchComponent } from '../../shared/header-search/header-search.component';
import { HeaderOrgSearchComponent } from '../../shared/header-org-search/header-org-search.component';
import { LeftNavigationComponent } from '../../shared/left-navigation/left-navigation.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import { TextboxComponent } from '../../components/textbox/textbox.component';
import { CheckboxComponent } from '../../components/checkbox/checkbox.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { NgxMasonryModule } from 'ngx-masonry';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { StandardOrgSelectionComponent } from '../../components/standard-org-selection/standard-org-selection.component';
import { RecentStandardsListComponent } from '../../components/recent-standards-list/recent-standards-list.component';
import { StandardAvailableDetailComponent } from '../../components/standard-available-detail/standard-available-detail.component';
import { L10nConfig, L10nLoader, LocalizationModule, StorageStrategy, ProviderType } from 'angular-l10n';

import { StandardsOrgSearchComponent } from './standards-org-search.component';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'fr', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

describe('StandardsOrgSearchComponent', () => {
  let component: StandardsOrgSearchComponent;
  let fixture: ComponentFixture<StandardsOrgSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardsOrgSearchComponent, HeaderComponent, HeaderSearchComponent,
                      DropdownComponent, TextboxComponent, CheckboxComponent,
                      HeaderOrgSearchComponent, LeftNavigationComponent, FooterComponent,
                      StandardOrgSelectionComponent, RecentStandardsListComponent,
                      StandardAvailableDetailComponent ],
      imports: [
        FormsModule,
        NgxMasonryModule,
        HttpClientModule,
        RouterTestingModule,
        LocalizationModule.forRoot(l10nConfig),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardsOrgSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
