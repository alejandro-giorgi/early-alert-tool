import { Component, OnInit } from '@angular/core';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-keyword-search',
  templateUrl: './keyword-search.component.html',
  styleUrls: ['./keyword-search.component.scss']
})
export class KeywordSearchComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  results: Array<any>;
  divisionId: string;
  subDivisionId: number;
  productLineId: number;
  organizationId: number;
  standardId: number;
  email: string;
  keyword: string;
  totalRows: number;
  totalPages: number;
  pageLimit =  10;
  activePage = {
    'pageIndex': 1
  };
  isSearching = false;
  isMail: any;
  isOpenModal: any;
  modalType = 'print';
  constructor(
    private keywordSearchService: KeywordSearchService
  ) { }

  ngOnInit() {
    this.getResults();
  }

  setPage(pageNum: number) {
    this.keywordSearchService.getPageData('standardDivisions', pageNum).subscribe(data => {
      this.results = data;
    });
    window.scrollTo(0, 0);
  }

  // data get from service
  getResults(): void {
    this.keywordSearchService.getPageData('standardDivisions', this.activePage['pageIndex']).subscribe(data => {
      this.results = data;
    });
    this.keywordSearchService.getData('standardDivisions').subscribe(data => {
      this.totalPages = data.length / this.pageLimit;
      this.totalRows = data.length;
      this.isSearching = false;
    });
  }

  // Filter and search
  getQueryResult(obj): void {
    this.keywordSearchService.searchResult(
      'standardDivisions',
      obj.divisonId,
      obj.subDivisionId,
      obj.productLineId,
      obj.organizationId,
      obj.standardId,
      obj.email,
      obj.keyword
    ).subscribe(data => {
      this.results = data;
      this.activePage['pageIndex'] = 1;
      this.isSearching = true;
    });
  }
}
