import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  results: Array<any>;
  totalRows: number;
  totalPages: number;
  pageLimit =  10;
  activePage = {
    'pageIndex': 1
  };
  isSearching = false;
  isOpenModal: any;
  modalType = '';
  rowData: any;

  constructor(
    private keywordSearchService: KeywordSearchService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getResults();

    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    } else if (userRole !== 'SystemAdmin' && userRole !== 'AdminApprover') {
      this.router.navigate(['/keyword-search']);
    }
  }

  // delete row
  setRowData(event) {
    this.keywordSearchService.getUserData('users/' + event).subscribe(data => {
      this.rowData = data;

      this.isOpenModal = true;
    });
  }

  setPage(pageNum: number) {
    this.keywordSearchService.getUserPageData('users', pageNum).subscribe(data => {
      this.results = data;
    });
    window.scrollTo(0, 0);
  }

  // data get from service
  getResults(): void {
    this.isOpenModal = false;

    this.keywordSearchService.getUserPageData('users', this.activePage['pageIndex']).subscribe(data => {
      this.results = data;
    });
    this.keywordSearchService.getUserData('users').subscribe(data => {
      this.totalPages = Math.ceil(data.length / this.pageLimit);
      this.totalRows = data.length;
      this.isSearching = false;
    });
  }

  // Filter and search
  getQueryResult(obj): void {
    this.keywordSearchService.searchResultUserKeyword(
      'users',
      obj.keyword
    ).subscribe(data => {
      this.results = data;
      this.totalRows = data.length;
      this.activePage['pageIndex'] = 1;
      this.isSearching = true;
    });
  }
}
