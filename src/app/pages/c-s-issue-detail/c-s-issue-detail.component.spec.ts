import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HeaderSearchComponent } from '../../shared/header-search/header-search.component';
import { HeaderOrgSearchComponent } from '../../shared/header-org-search/header-org-search.component';
import { LeftNavigationComponent } from '../../shared/left-navigation/left-navigation.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import { TextboxComponent } from '../../components/textbox/textbox.component';
import { CSIssuesDetailPanelComponent } from '../../components/c-s-issues-detail-panel/c-s-issues-detail-panel.component';
import { ModelComponent } from '../../components/model/model.component';
import { CheckboxComponent } from '../../components/checkbox/checkbox.component';
import { SortablejsModule } from 'angular-sortablejs';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nConfig, L10nLoader, LocalizationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { LoginComponent } from '../login/login.component';

import { CSIssueDetailComponent } from './c-s-issue-detail.component';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'fr', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

describe('CSIssueDetailComponent', () => {
  let component: CSIssueDetailComponent;
  let fixture: ComponentFixture<CSIssueDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSIssueDetailComponent, HeaderComponent,
                      HeaderSearchComponent, HeaderOrgSearchComponent,
                      LeftNavigationComponent, FooterComponent, CheckboxComponent,
                      CSIssuesDetailPanelComponent, ModelComponent,
                      DropdownComponent, TextboxComponent, LoginComponent ],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent}
        ]),
        LocalizationModule.forRoot(l10nConfig),
        SortablejsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSIssueDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
