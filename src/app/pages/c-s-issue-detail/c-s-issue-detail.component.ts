import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-c-s-issue-detail',
  templateUrl: './c-s-issue-detail.component.html',
  styleUrls: ['./c-s-issue-detail.component.scss']
})
export class CSIssueDetailComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  issueDetail = {division: {}};
  isOpenModal: any;
  modalType = '';

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    }

    this.route.params.subscribe(params => {
      if ((params['id'] !== undefined) && (params['isTop'] !== undefined)) {
        this.dataService.getData('CnSIssue' + (params['isTop'] === 'true' ? 'Top' : '') + '/' + params['id']).subscribe(data => {
          this.issueDetail = data;
        });
      }
    });
  }

  // print
  print() {
    this.isOpenModal = true;
    this.modalType = 'print';
  }
}
