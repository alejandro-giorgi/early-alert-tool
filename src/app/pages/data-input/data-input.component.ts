import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-input',
  templateUrl: './data-input.component.html',
  styleUrls: ['./data-input.component.scss']
})
export class DataInputComponent implements OnInit {
  activeTab = 1;
  isOffcanvas = {
    value: true
  };
  constructor(private router: Router) { }

  ngOnInit() {
    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    } else if (userRole !== 'SystemAdmin' && userRole !== 'DataManager' && userRole !== 'AdminApprover') {
      this.router.navigate(['/keyword-search']);
    }
  }

  toggleForm(value: number) {
    this.activeTab = value;
  }
}
