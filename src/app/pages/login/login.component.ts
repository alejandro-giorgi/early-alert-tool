import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData = {
    userName: '',
    passWord: '',
  };
  remember = false;
  showError = false;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    if (sessionStorage.getItem('user')) {
      this.router.navigate(['/keyword-search']);
    }
  }

  login() {
    this.dataService.setData('login', this.loginData).subscribe(data => {
      if (Object.keys(data).length > 0) {
        this.showError = false;
        sessionStorage.setItem('user',  data.role.name);
        this.router.navigate(['/keyword-search']);
      } else {
        this.showError = true;
      }
    });

  }

}
