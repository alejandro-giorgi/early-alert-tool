import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HeaderSearchComponent } from '../../shared/header-search/header-search.component';
import { HeaderOrgSearchComponent } from '../../shared/header-org-search/header-org-search.component';
import { LeftNavigationComponent } from '../../shared/left-navigation/left-navigation.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { PaginationComponent } from '../../components/pagination/pagination.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import { TextboxComponent } from '../../components/textbox/textbox.component';
import { CSIssuesTableComponent } from '../../components/c-s-issues-table/c-s-issues-table.component';
import { ModelComponent } from '../../components/model/model.component';
import { CheckboxComponent } from '../../components/checkbox/checkbox.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nConfig, L10nLoader, LocalizationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { LoginComponent } from '../login/login.component';

import { CSIssuesComponent } from './c-s-issues.component';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'fr', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

describe('CSIssuesComponent', () => {
  let component: CSIssuesComponent;
  let fixture: ComponentFixture<CSIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSIssuesComponent, HeaderComponent,
                      HeaderSearchComponent, HeaderOrgSearchComponent,
                      LeftNavigationComponent, FooterComponent,
                      ModelComponent, CSIssuesTableComponent,
                      DropdownComponent, TextboxComponent,
                      PaginationComponent, CheckboxComponent, LoginComponent ],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent}
        ]),
        LocalizationModule.forRoot(l10nConfig),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
