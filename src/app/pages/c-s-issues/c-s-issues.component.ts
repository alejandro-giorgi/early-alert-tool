import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../service/data.service';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-c-s-issues',
  templateUrl: './c-s-issues.component.html',
  styleUrls: ['./c-s-issues.component.scss']
})
export class CSIssuesComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  topResults: Array<any>;
  results: Array<any>;

  searchTxt = '';
  totalRows: number;
  totalPages: number;
  pageLimit =  10;
  activePage = {
    'pageIndex': 1
  };
  isSearching = false;
  isOpenModal: any;
  modalType = '';
  showReadAll = {
    'shown': true,
    'checked': false,
    'unreadTopNumber': 0,
    'unreadNumber': 0
  };

  regionOptions: Array<string> = ['Any'];
  divsionOptions: Array<string> = ['Any'];
  regionSelected = 'Any';
  divsionSelected = 'Any';
  Any = 'Any';

  constructor(
    private dataService: DataService,
    private keywordSearchService: KeywordSearchService,
    private router: Router
  ) { }

  ngOnInit() {
    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    }

    this.getResults();

    this.getRegionOptions();
    this.getDivsionOptions();
  }

  // read all
  readAll(event) {
    this.topResults.forEach((item) => {
      if (!item.read) {
        item['read'] = true;
        this.dataService.updateData('CnSIssueTop/' + item['id'], item).subscribe(data => {
          this.getResults();
        });
      }
    });

    this.results.forEach((item) => {
      if (!item.read) {
        item['read'] = true;
        this.dataService.updateData('CnSIssue/' + item['id'], item).subscribe(data => {
          this.getResults();
        });
      }
    });
  }

  // drop down value selected
  onSelect(prop, value: any) {
    this[prop] = value;

    this.getQueryResult();
  }

  setPage(pageNum: number) {
    this.keywordSearchService.getPageData('CnSIssue', pageNum).subscribe(data => {
      this.results = data;
    });
    window.scrollTo(0, 0);
  }

  // data get from Region
  getRegionOptions(): void {
    this.keywordSearchService.getData('divisions').subscribe(data => {
      data.forEach((item) => {
        this.regionOptions.push(item.region);
      });
    });
  }

  // data get from division
  getDivsionOptions(): void {
    this.keywordSearchService.getData('divisions').subscribe(data => {
      data.forEach((item) => {
        this.divsionOptions.push(item.name);
      });
    });
  }

  // data get from service
  getResults(): void {
    this.keywordSearchService.getPageData('CnSIssueTop', 1).subscribe(data => {
      this.topResults = data;
      this.showReadAll['unreadTopNumber'] = 0;
      this.topResults.forEach((item) => {
        if (!item.read) {
          this.showReadAll['unreadTopNumber']++;
        }
      });
    });
    this.keywordSearchService.getPageData('CnSIssue', this.activePage['pageIndex']).subscribe(data => {
      this.results = data;
      this.showReadAll['unreadNumber'] = 0;
      this.results.forEach((item) => {
        if (!item.read) {
          this.showReadAll['unreadNumber']++;
        }
      });
    });
    this.keywordSearchService.getData('CnSIssue').subscribe(data => {
      this.totalPages = Math.ceil(data.length / this.pageLimit);
      this.totalRows = data.length;
      this.isSearching = false;
    });
  }

  // Filter and search
  getQueryResult(): void {
    this.keywordSearchService.searchResultKeywordGroupDivision(
      'CnSIssue',
      this.searchTxt,
      this.regionSelected,
      this.divsionSelected
    ).subscribe(data => {
      this.results = data;
      this.totalRows = data.length;
      this.activePage['pageIndex'] = 1;
      this.isSearching = true;
    });
  }
}
