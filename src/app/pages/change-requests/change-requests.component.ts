import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-change-requests',
  templateUrl: './change-requests.component.html',
  styleUrls: ['./change-requests.component.scss']
})
export class ChangeRequestsComponent implements OnInit {
  isOffcanvas = {
    value: true
  };
  results: Array<any>;
  totalRows: number;
  totalPages: number;
  pageLimit =  10;
  activePage = {
    'pageIndex': 1
  };
  isSearching = false;
  isOpenModal: any;
  modalType = '';
  constructor(
    private keywordSearchService: KeywordSearchService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getResults();

    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    } else if (userRole !== 'AdminApprover') {
      this.router.navigate(['/keyword-search']);
    }
  }

  setPage(pageNum: number) {
    this.keywordSearchService.getPageData('changeRequests', pageNum).subscribe(data => {
      this.results = data;
    });
    window.scrollTo(0, 0);
  }

  // data get from service
  getResults(): void {
    this.keywordSearchService.getPageData('changeRequests', this.activePage['pageIndex']).subscribe(data => {
      this.results = data;
    });
    this.keywordSearchService.getData('changeRequests').subscribe(data => {
      this.totalPages = Math.ceil(data.length / this.pageLimit);
      this.totalRows = data.length;
      this.isSearching = false;
    });
  }

  // Filter and search
  getQueryResult(obj): void {
    this.keywordSearchService.searchResultKeyword(
      'changeRequests',
      obj.keyword
    ).subscribe(data => {
      this.results = data;
      this.totalRows = data.length;
      this.activePage['pageIndex'] = 1;
      this.isSearching = true;
    });
  }
}
