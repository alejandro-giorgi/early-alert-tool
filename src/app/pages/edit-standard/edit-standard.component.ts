import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-standard',
  templateUrl: './edit-standard.component.html',
  styleUrls: ['./edit-standard.component.scss']
})
export class EditStandardComponent implements OnInit {
  activeTab = 1;
  isOffcanvas = {
    value: true
  };
  standardTitle = '';
  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const userRole = sessionStorage.getItem('user');
    if (!userRole) {
      this.router.navigate(['/login']);
    }

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.dataService.getData('standardDivisions/' + params['id']).subscribe(data => {
          this.standardTitle = data['standard']['name'];
        });
      }
    });
  }
}
