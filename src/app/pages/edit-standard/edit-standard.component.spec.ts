import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from '../../shared/header/header.component';
import { HeaderSearchComponent } from '../../shared/header-search/header-search.component';
import { HeaderOrgSearchComponent } from '../../shared/header-org-search/header-org-search.component';
import { LeftNavigationComponent } from '../../shared/left-navigation/left-navigation.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import { TextboxComponent } from '../../components/textbox/textbox.component';
import { DataInputFormsComponent } from '../../components/data-input-forms/data-input-forms.component';
import { DatePickerComponent } from '../../components/date-picker/date-picker.component';
import { TextareaComponent } from '../../components/textarea/textarea.component';
import { InputTagComponent } from '../../components/input-tag/input-tag.component';
import { CheckboxComponent } from '../../components/checkbox/checkbox.component';
import { TagInputModule } from 'ngx-chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FooterComponent } from '../../shared/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nConfig, L10nLoader, LocalizationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { LoginComponent } from '../login/login.component';

import { EditStandardComponent } from './edit-standard.component';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'fr', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

describe('EditStandardComponent', () => {
  let component: EditStandardComponent;
  let fixture: ComponentFixture<EditStandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStandardComponent, HeaderComponent,
                      HeaderSearchComponent, HeaderOrgSearchComponent,
                      LeftNavigationComponent, FooterComponent,
                      DropdownComponent, TextboxComponent,
                      DataInputFormsComponent, CheckboxComponent,
                      DatePickerComponent, InputTagComponent,
                      TextareaComponent, LoginComponent ],
      imports: [
        FormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent}
        ]),
        LocalizationModule.forRoot(l10nConfig),
        TagInputModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
