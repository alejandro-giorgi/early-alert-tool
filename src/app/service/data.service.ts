import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = environment.apiBase;  // URL to web api

  constructor(private http: HttpClient) { }

  /** GET heroes from the server */
  getData(url): Observable<any> {
    const fullUrl = `${this.apiUrl}${url}`;
    return this.http.get<any>(fullUrl)
      .pipe(
        tap(response => response),
        catchError(this.handleError('getData', []))
      );
  }

  /** POST call setData */
  setData(url, data): Observable<any> {
    const fullUrl = `${this.apiUrl}${url}`;
    return this.http.post<any>(fullUrl, data, { headers: {'Content-Type': 'application/json'} })
      .pipe(
        tap(response => response),
        catchError(this.handleError('getData', []))
      );
  }

  /** POST call setData */
  updateData(url, data): Observable<any> {
    const fullUrl = `${this.apiUrl}${url}`;
    return this.http.put<any>(fullUrl, data, { headers: {'Content-Type': 'application/json'} })
      .pipe(
        tap(response => response),
        catchError(this.handleError('getData', []))
      );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
