import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left-navigation',
  templateUrl: './left-navigation.component.html',
  styleUrls: ['./left-navigation.component.scss']
})
export class LeftNavigationComponent implements OnInit {
  @Input() isOffcanvas = {
    value: true
  };
  @Output() offcanvas = new EventEmitter();
  isOpen = true;
  userRole = '';
  isLoggedIn = false;

  ngOnInit() {
    if (!sessionStorage.getItem('user')) {
      this.isLoggedIn = false;
    } else {
      this.isLoggedIn = true;
    }
    this.userRole = sessionStorage.getItem('user');
  }

  toggle(event: any) {
    this.isOpen = !this.isOpen;
    this.isOffcanvas['value'] = !this.isOffcanvas['value'];
  }
}
