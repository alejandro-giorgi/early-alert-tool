import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header-org-search',
  templateUrl: './header-org-search.component.html',
  styleUrls: ['./header-org-search.component.scss']
})
export class HeaderOrgSearchComponent implements OnInit {
  @Input() tabs = [];
  @Output() sendTab = new EventEmitter();
  @Output() sendActive = new EventEmitter();
  activeTab = 0;
  tabsLists = {};

  ngOnInit() {
    this.sendTab.emit(this.tabs);
  }

  // add new tab to tabs list
  addTab() {
    this.tabs.push({
      name: 'Standard ' + (this.tabs.length + 1)
    });
    this.sendTab.emit(this.tabs);
  }

  // switch between tabs
  changeTab(idx: number) {
    this.activeTab = idx;
    this.sendActive.emit(this.activeTab);
  }

  // remove tab from tabs list
  removeTab(idx: number) {
    this.tabs.splice(idx, 1);
  }
}
