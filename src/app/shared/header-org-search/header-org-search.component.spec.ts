import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOrgSearchComponent } from './header-org-search.component';

describe('HeaderOrgSearchComponent', () => {
  let component: HeaderOrgSearchComponent;
  let fixture: ComponentFixture<HeaderOrgSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderOrgSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOrgSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
