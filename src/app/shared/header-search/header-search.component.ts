import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { findIndex } from 'lodash';

import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.scss']
})
export class HeaderSearchComponent implements OnInit {
  @Output() getFilter = new EventEmitter();
  standards: any;
  divisions: any;
  subDivisions: any;
  productLines: any;
  organizations: any;
  searchTxt = '';

  sdOptions: Array<string> = ['Any'];
  divsionOptions: Array<string> = ['Any'];
  subDivsionOptions: Array<string> = ['Any'];
  productOptions: Array<string> = ['Any'];
  orgOptions: Array<string> = ['Any'];

  divisionId: string;
  subDivisionId: number;
  productLineId: number;
  organizationId: number;
  standardId: number;
  emails = '';
  keyword = '';

  Any = 'Any';

  queryObj: any = {};
  constructor(
    private keywordSearchService: KeywordSearchService
  ) { }

  ngOnInit() {
    this.getOptionSD();
    this.getdivsionOptions();
    this.getSubDivsionOptions();
    this.getProductOptions();
    this.getOrgOptions();
  }

  // reset selected oprions
  reset() {
    // set emails, keywordSearch ... etc;
    this.queryObj = {};
    this.searchTxt = '';
    this.keyword = '';
    this.emails = '';
  }

  // data get from standars
  getOptionSD(): void {
    this.keywordSearchService.getData('standards').subscribe(data => {
      this.standards = data;
      data.forEach((item) => {
        this.sdOptions.push(item.description);
      });
    });
  }

  // data get from division
  getdivsionOptions(): void {
    this.keywordSearchService.getData('divisions').subscribe(data => {
      this.divisions = data;
      data.forEach((item) => {
        this.divsionOptions.push(item.name);
      });
    });
  }

  // data get from subdivisions
  getSubDivsionOptions(): void {
    this.keywordSearchService.getData('subDivisions').subscribe(data => {
      this.subDivisions = data;
      data.forEach((item) => {
        this.subDivsionOptions.push(item.name);
      });
    });
  }

  // data get from getProductLine
  getProductOptions(): void {
    this.keywordSearchService.getData('productLines').subscribe(data => {
      this.productLines = data;
      data.forEach((item) => {
        this.productOptions.push(item.name);
      });
    });
  }

  // data get from orgoption
  getOrgOptions(): void {
    this.keywordSearchService.getData('organizations').subscribe(data => {
      this.organizations = data;
      data.forEach((item) => {
        this.orgOptions.push(item.name);
      });
    });
  }

  // onFilter
  onSearch() {
    this.queryObj['email'] = this.emails;
    this.queryObj['keyword'] = this.searchTxt;
    this.getFilter.emit(this.queryObj);
  }

  /**
  * option selection
  * @param name - Selected option
  */
  onSelect(prop: string, arrName: string, findKey: string, value: string): void {
    const idx = findIndex(this[arrName], {[findKey]: value});
    // set standardId, divisonId ... etc;
    this.queryObj[prop] = this[arrName][idx].id;
  }

}
