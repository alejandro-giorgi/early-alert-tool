import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { LocaleService, TranslationService, Language } from 'angular-l10n';
import { findIndex } from 'lodash';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() pageType: string;
  @Input() title: string;
  @Input() tabs: any;
  @Output() getFilter = new EventEmitter();
  @Output() sendTab = new EventEmitter();
  @Output() sendActive = new EventEmitter();

  isLoggedIn = false;
  showNotify = false;
  notifyData = [];

  subscription: Subscription;
  languages: any;
  langOptions = [];
  selectedLaguage = 'English';
  selectedLaguageCode = 'en';
  constructor(
    private router: Router,
    private location: Location,
    public locale: LocaleService,
    public translation: TranslationService,
    public pageTitle: Title,
    private dataService: DataService
  ) { }

  ngOnInit() {
    if (!sessionStorage.getItem('user')) {
      this.isLoggedIn = false;
    } else {
      this.isLoggedIn = true;
    }
    // get already selected language from cookie
    const cokkieSplit = document.cookie.match(new RegExp('(^| )' + 'defaultLocale' + '=([^;]+)'));
    if (cokkieSplit) {
      this.selectedLaguageCode = cokkieSplit[2].split('-')[1];
    }

    this.getLanguage();
    this.getData();
    this.getNotifyData();
  }

  // data get from service
  getNotifyData(): void {
    this.dataService.getData('notifications').subscribe(data => {
      this.notifyData = data;
    });
  }

  // data get from service
  getData(): void {
    this.dataService.getData('languages').subscribe(data => {
      this.languages = data;
      this.languages.forEach((item, idx) => {
        this.langOptions.push(item.name);
        if (this.selectedLaguageCode === item.countryCode) {
          this.selectedLaguage = item.name;
        }
      });
    });
  }

  // get language while pageload
  getLanguage() {
    // When the language changes, refreshes the document title with the new translation.
    this.subscription = this.translation.translationChanged().subscribe(
        () => { this.pageTitle.setTitle(this.translation.translate('Title')); }
    );
  }

  // onFilter
  onFilter(obj) {
    this.getFilter.emit(obj);
  }

  // click Back button
  backPage() {
    this.location.back();
  }

  /**
  * option selection
  * @param name - Selected option
  */
  selectLocale(name: string): void {
    const idx = findIndex(this.languages, {name: name});
    this.locale.setDefaultLocale(this.languages[idx].code, this.languages[idx].countryCode);
  }

  // login
  login() {
    this.router.navigate(['/login']);
  }

  // logout
  logout() {
    sessionStorage.setItem('user',  '');
    this.router.navigate(['/login']);
  }

  // get tab from org tabs
  getTab(tab: any) {
    this.sendTab.emit(tab);
  }

  // get tab from org tabs
  getActive(activetab: number) {
    this.sendActive.emit(activetab);
  }

}
