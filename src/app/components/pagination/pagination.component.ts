import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() totalPages: number;
  @Input() activePage = {
    'pageIndex': 1
  };
  @Input() showReadAll = {
    'shown': false,
    'checked': false,
    'unreadTopNumber': 0,
    'unreadNumber': 0
  };
  @Output() goto = new EventEmitter();
  @Output() readAll = new EventEmitter();
  numbers = [];

  ngOnInit() {
    setTimeout(() => {
      for (let i = 1; i <= this.totalPages; i++) {
        this.numbers.push(i);
      }
    }, 1000);
  }

  // check number of the box
  readAllCheck(event) {
    this.showReadAll['checked'] = event;

    this.readAll.emit(true);
  }

  onGoto(idx: number) {
    this.activePage['pageIndex'] = idx + 1;
    this.goto.emit(this.activePage['pageIndex']);
  }

  onPrev() {
    this.activePage['pageIndex'] = this.activePage['pageIndex'] - 1;
    this.goto.emit(this.activePage['pageIndex']);
  }

  onNext() {
    this.activePage['pageIndex'] = this.activePage['pageIndex'] + 1;
    this.goto.emit(this.activePage['pageIndex']);
  }
}
