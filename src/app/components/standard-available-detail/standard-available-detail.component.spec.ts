import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { NgxMasonryModule } from 'ngx-masonry';
import { StandardAvailableDetailComponent } from './standard-available-detail.component';

describe('StandardAvailableDetailComponent', () => {
  let component: StandardAvailableDetailComponent;
  let fixture: ComponentFixture<StandardAvailableDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardAvailableDetailComponent, CheckboxComponent ],
      imports: [
        FormsModule,
        NgxMasonryModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardAvailableDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
