import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-standard-available-detail',
  templateUrl: './standard-available-detail.component.html',
  styleUrls: ['./standard-available-detail.component.scss']
})
export class StandardAvailableDetailComponent {
  @Input() divisions: any;
  @Input() activeStandard = {
    divisionsData: []
  };

  // check number of the box
  checkNumber(checkbox, box, event) {
    let number = 0;

    checkbox['checked'] = event;
    box['checkboxList'].forEach((item) => {
      if (item.checked) {
        number++;
      }
    });

    box['checkedNumber'] = number;
  }
}
