import { Component, OnInit, Input } from '@angular/core';
import { SortablejsOptions } from 'angular-sortablejs';

@Component({
  selector: 'app-c-s-issues-detail-panel',
  templateUrl: './c-s-issues-detail-panel.component.html',
  styleUrls: ['./c-s-issues-detail-panel.component.scss']
})
export class CSIssuesDetailPanelComponent {
  @Input() datas = {
    'description': ''
  };

  descriptionHide: boolean;
  financialImpactHide: boolean;
  actionPlanHide: boolean;
  offensiveDefensiveHide: boolean;
  impactSummaryHide: boolean;
  itemTypeHide: boolean;
  successionPlanHide: boolean;
  createdDateHide: boolean;
  externalPartnersHide: boolean;
  strategicPlanHide: boolean;

  optionsModule: SortablejsOptions = {
    animation: 150,
    handle: '.move-icon'
  };
}
