import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SortablejsModule } from 'angular-sortablejs';
import { CSIssuesDetailPanelComponent } from './c-s-issues-detail-panel.component';

describe('CSIssuesDetailPanelComponent', () => {
  let component: CSIssuesDetailPanelComponent;
  let fixture: ComponentFixture<CSIssuesDetailPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSIssuesDetailPanelComponent ],
      imports: [
        SortablejsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSIssuesDetailPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
