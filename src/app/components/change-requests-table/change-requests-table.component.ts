import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-change-requests-table',
  templateUrl: './change-requests-table.component.html'
})
export class ChangeRequestsTableComponent implements AfterViewChecked {
  @Input() datas: Array<any>;
  @Input() totalRows = 0;
  @Output() printing = new EventEmitter();
  @Output() getFilter = new EventEmitter();
  searchTxt = '';
  isThChk = false;

  ngAfterViewChecked() {
    setTimeout(() => {
      this.datas.forEach((item, idx) => {
        this['isTdChk' + idx] = false;
      });
    }, 1000);
  }

  // check & unCheck all
  thToggleCheck(value) {
    this.datas.forEach((item, idx) => {
      this.datas[idx].isChecked = value;
    });
  }

  // checkbox selection
  checkboxs(idx: number, value: boolean): void {
    // unCheck thead cheackbox, if any of body chkbox is uncheck
    this.datas[idx].isChecked = value;
    const isTh = this.datas.every((item) => {
      return item.isChecked;
    });
    this.isThChk = isTh;
  }

  // print
  print() {
    this.printing.emit(true);
  }

  // onFilter
  onFilter() {
    const obj = {};
    obj['keyword'] = this.searchTxt;
    this.getFilter.emit(obj);
  }
}
