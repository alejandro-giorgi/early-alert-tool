import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { ChangeRequestsTableComponent } from './change-requests-table.component';

describe('ChangeRequestsTableComponent', () => {
  let component: ChangeRequestsTableComponent;
  let fixture: ComponentFixture<ChangeRequestsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRequestsTableComponent, TextboxComponent, CheckboxComponent ],
      imports: [
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRequestsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
