import { Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-navigation-tab',
  templateUrl: './navigation-tab.component.html',
  styleUrls: ['./navigation-tab.component.scss']
})
export class NavigationTabComponent {
  @Output() toggleNav = new EventEmitter();

  activeTab = 1;

  toggleMenu(value: number) {
    this.activeTab = value;
    this.toggleNav.emit(value);
  }
}
