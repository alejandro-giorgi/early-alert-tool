import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss']
})
export class ModelComponent implements OnInit {
  @Input() modalType = '';
  @Input() rowData: any;
  @Output() close = new EventEmitter();
  @Output() reload = new EventEmitter();

  roleOptions: Array<string> = [];

  emailList = [
    'contactme@thor.com',
    'contactme@thor.com',
    'contactme@thor.com',
    'contactme@thor.com'
  ];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.getRoleOptions();

    if (this.modalType === 'create-new-user') {
      this.rowData = {role: {}, status: 'active'};
    }
  }

  cancel() {
    this.close.emit(false);
  }

  // confirm delete user
  confirmDeleteUser() {
    this.rowData['status'] = 'inactive';
    this.dataService.updateData('users/' + this.rowData['id'], this.rowData).subscribe(data => {
      this.reload.emit(true);
    });
  }

  // update user
  updateUser() {
    this.dataService.updateData('users/' + this.rowData['id'], this.rowData).subscribe(data => {
      this.reload.emit(true);
    });
  }

  // add user
  addUser() {
    this.dataService.setData('users', this.rowData).subscribe(data => {
      this.reload.emit(true);
    });
  }

  // drop down value added in model
  onSelectRole(value: any) {
    this.rowData['role']['name'] = value;
  }

  // data get from orgoption
  getRoleOptions(): void {
    this.dataService.getData('userRole').subscribe(data => {
      data.forEach((item) => {
        this.roleOptions.push(item.name);
      });
    });
  }
}
