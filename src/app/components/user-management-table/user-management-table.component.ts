import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-user-management-table',
  templateUrl: './user-management-table.component.html'
})
export class UserManagementTableComponent implements AfterViewChecked {
  @Input() datas: Array<any>;
  @Input() totalRows = 0;
  @Output() printing = new EventEmitter();
  @Output() adding = new EventEmitter();
  @Output() editing = new EventEmitter();
  @Output() deleting = new EventEmitter();
  @Output() getFilter = new EventEmitter();
  searchTxt = '';
  isThChk = false;
  isModal = false;
  activeIndex = -1;

  ngAfterViewChecked() {
    setTimeout(() => {
      this.datas.forEach((item, idx) => {
        this['isTdChk' + idx] = false;
      });
    }, 1000);
  }

  // check & unCheck all
  thToggleCheck(value) {
    this.datas.forEach((item, idx) => {
      this.datas[idx].isChecked = value;
    });
  }

  // checkbox selection
  checkboxs(idx: number, value: boolean): void {
    // unCheck thead cheackbox, if any of body chkbox is uncheck
    this.datas[idx].isChecked = value;
    const isTh = this.datas.every((item) => {
      return item.isChecked;
    });
    this.isThChk = isTh;
  }

  // print
  print() {
    this.printing.emit(true);
  }

  // add row
  addRow() {
    this.adding.emit(true);
  }

  // edit row
  editRow(id) {
    this.editing.emit(id);
  }

  // delete row
  deleteRow(id) {
    this.deleting.emit(id);
  }

  // onFilter
  onFilter() {
    const obj = {};
    obj['keyword'] = this.searchTxt;
    this.getFilter.emit(obj);
  }
}
