import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { findIndex } from 'lodash';
import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-standard-org-selection',
  templateUrl: './standard-org-selection.component.html',
  styleUrls: ['./standard-org-selection.component.scss']
})
export class StandardOrgSelectionComponent implements OnInit {
  @Output() standard = new EventEmitter();
  standards: any;
  standards_shown: any;
  organizations: any;

  orgOptions: Array<string> = [];

  organizationId: number;
  activeStd = -1;
  searchText = '';

  constructor(
    private keywordSearchService: KeywordSearchService
  ) { }

  ngOnInit() {
    this.getOrgOptions();
  }

  // data get from getProductLine
  getOrgOptions(): void {
    this.keywordSearchService.getData('organizations').subscribe(data => {
      this.organizations = data;
      data.forEach((item) => {
        this.orgOptions.push(item.name);
      });
    });
  }

  // data get from standars
  getOptionSD(): void {
    this.keywordSearchService.getStandardsById(this.organizationId).subscribe(data => {
      this.standards = data;
      this.standards_shown = data;
      this.searchText = '';
    });
  }

  /**
  * option selection
  * @param name - Selected option
  */
  onSelect(prop: string, arrName: string, findKey: string, value: string): void {
    const idx = findIndex(this[arrName], {[findKey]: value});
    // set standardId, divisonId ... etc;
    this[prop] = this[arrName][idx].id;
    if (prop === 'organizationId') {
      this.getOptionSD();
      this.searchText = '';
    }
  }

  /**
  * keyword type
  */
  onSearch() {
    this.standards_shown = [];

    if ((this.standards === undefined) || (this.searchText === '')) {
      this.standards_shown = this.standards;
      return;
    }

    this.standards.forEach((item) => {
      if (item['name'].toLowerCase().indexOf(this.searchText.toLowerCase()) >= 0
       || item['description'].toLowerCase().indexOf(this.searchText.toLowerCase()) >= 0 ) {
         this.standards_shown.push(item);
      }
    });
  }

  /**
  * option selection
  * @param index - Selected option
  */
  selectStandard(idx) {
    this.activeStd = idx;
    this.standard.emit(this.standards[idx]);
  }
}
