import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { HttpClientModule } from '@angular/common/http';
import { StandardOrgSelectionComponent } from './standard-org-selection.component';

describe('StandardOrgSelectionComponent', () => {
  let component: StandardOrgSelectionComponent;
  let fixture: ComponentFixture<StandardOrgSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardOrgSelectionComponent, TextboxComponent, DropdownComponent ],
      imports: [
        FormsModule,
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardOrgSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
