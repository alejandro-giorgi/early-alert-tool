import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchResultsTableComponent } from './search-results-table.component';

describe('SearchResultsTableComponent', () => {
  let component: SearchResultsTableComponent;
  let fixture: ComponentFixture<SearchResultsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchResultsTableComponent, TextboxComponent, CheckboxComponent ],
      imports: [
        RouterTestingModule,
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
