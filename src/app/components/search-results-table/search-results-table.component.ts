import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-search-results-table',
  templateUrl: './search-results-table.component.html'
})
export class SearchResultsTableComponent implements OnInit, AfterViewChecked {
  @Input() datas: Array<any>;
  @Input() totalRows = 0;
  @Output() printing = new EventEmitter();
  @Output() deleting = new EventEmitter();
  @Output() mailOpen = new EventEmitter();
  isThChk = false;
  isModal = false;
  isLoggedIn = false;
  activeIndex = -1;

  ngOnInit() {
    if (!sessionStorage.getItem('user')) {
      this.isLoggedIn = false;
    } else {
      this.isLoggedIn = true;
    }
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.datas.forEach((item, idx) => {
        this['isTdChk' + idx] = false;
      });
    }, 1000);
  }

  // check & unCheck all
  thToggleCheck(value) {
    this.datas.forEach((item, idx) => {
      this.datas[idx].isChecked = value;
    });
  }

  // checkbox selection
  checkboxs(idx: number, value: boolean): void {
    // unCheck thead cheackbox, if any of body chkbox is uncheck
    this.datas[idx].isChecked = value;
    const isTh = this.datas.every((item) => {
      return item.isChecked;
    });
    this.isThChk = isTh;
  }

  // print
  print() {
    this.printing.emit(true);
  }

  // delete row
  deleteRow() {
    this.deleting.emit(true);
  }

  // open more email in table
  openMore(idx: number, e) {
    e.stopPropagation();
    this.activeIndex = idx;
    this.mailOpen.emit(idx);
  }

  // close more email in table
  closeMore(idx: number, e) {
    this.activeIndex = idx;
    this.mailOpen.emit(idx);
  }

  /**
  * dropdown click on outside
  */
  @HostListener('document: click', ['$event'])
  onClick() {
    this.activeIndex = -1;
  }
}
