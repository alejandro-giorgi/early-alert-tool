import { Component, OnInit, AfterViewChecked, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-c-s-issues-table',
  templateUrl: './c-s-issues-table.component.html',
  styleUrls: ['./c-s-issues-table.component.scss']
})
export class CSIssuesTableComponent implements AfterViewChecked {
  @Input() topDatas: Array<any>;
  @Input() datas: Array<any>;
  @Input() totalRows = 0;
  @Input() showReadAll = {
    'shown': true,
    'checked': false,
    'unreadTopNumber': 0,
    'unreadNumber': 0
  };
  @Output() printing = new EventEmitter();
  @Output() reload = new EventEmitter();
  isTopThChk = false;
  isThChk = false;
  isModal = false;
  showOnlyMe = false;

  activeIndex = -1;

  ngAfterViewChecked() {
    setTimeout(() => {
      this.topDatas.forEach((item, idx) => {
        this['isTopTdChk' + idx] = false;
      });

      this.datas.forEach((item, idx) => {
        this['isTdChk' + idx] = false;
      });
    }, 1000);
  }

  // check & unCheck all on top
  thTopToggleCheck(value) {
    this.topDatas.forEach((item, idx) => {
      this.topDatas[idx].isChecked = value;
    });
  }

  // check & unCheck all
  thToggleCheck(value) {
    this.datas.forEach((item, idx) => {
      this.datas[idx].isChecked = value;
    });
  }

  // checkbox selection on top
  checkboxsTop(idx: number, value: boolean): void {
    // unCheck thead cheackbox, if any of body chkbox is uncheck
    this.topDatas[idx].isChecked = value;
    const isTh = this.topDatas.every((item) => {
      return item.isChecked;
    });
    this.isTopThChk = isTh;
  }

  // checkbox selection
  checkboxs(idx: number, value: boolean): void {
    // unCheck thead cheackbox, if any of body chkbox is uncheck
    this.datas[idx].isChecked = value;
    const isTh = this.datas.every((item) => {
      return item.isChecked;
    });
    this.isThChk = isTh;
  }

  // print
  print() {
    this.printing.emit(true);
  }

  // check number of the box
  checkShowOnlyMe(event) {
    this.showOnlyMe = event;
    this.reload.emit(true);
  }

  // open more email in table
  openMore(idx: number, e) {
    e.stopPropagation();
    this.activeIndex = idx;
  }

  /**
  * dropdown click on outside
  */
  @HostListener('document: click', ['$event'])
  onClick() {
    this.activeIndex = -1;
  }
}
