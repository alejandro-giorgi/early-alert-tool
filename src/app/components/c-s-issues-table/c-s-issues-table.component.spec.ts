import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CSIssuesTableComponent } from './c-s-issues-table.component';

describe('CSIssuesTableComponent', () => {
  let component: CSIssuesTableComponent;
  let fixture: ComponentFixture<CSIssuesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSIssuesTableComponent, TextboxComponent, CheckboxComponent ],
      imports: [
        RouterTestingModule,
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSIssuesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
