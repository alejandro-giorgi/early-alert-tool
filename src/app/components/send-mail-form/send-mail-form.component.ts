import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { KeywordSearchService } from '../../service/keyword-search.service';

@Component({
  selector: 'app-send-mail-form',
  templateUrl: './send-mail-form.component.html',
  styleUrls: ['./send-mail-form.component.scss']
})
export class SendMailFormComponent implements OnInit {
  @Output() sending = new EventEmitter();
  sendStep = 0;

  // quill-editor customOptions
  customOptions =  {
    toolbar: [
      [{ 'align': [] }],
      ['bold', 'italic', 'underline'],
      [{ 'color': [] }],
      ['link', 'image', 'video'],
    ]
  };
  emails = '';

  standardsSelected = [{
    'newAdded': false,
    'id': '2.5',
    'division': 'CDPAD'
  }];

  stOptions: Array<string> = [];
  stList: Array<string> = [];
  orgOptions: Array<string> = [];

  constructor(
    private keywordSearchService: KeywordSearchService
  ) { }

  ngOnInit() {
    this.getOptionSD();
    this.getOrgOptions();
  }

  // click Send button
  clickSend() {
    if (this.sendStep === 0) {
      this.sending.emit('email-failed');
      this.sendStep++;
    } else if (this.sendStep === 1) {
      this.sending.emit('email-success');
    }
  }

  // add new item
  addItem() {
    this.standardsSelected.push({
      'newAdded': true,
      'id': '',
      'division': ''
    });
  }

  // remove item
  removeItem(index) {
    this.standardsSelected.splice(index, 1);
  }

  // data get from standars
  getOptionSD(): void {
    this.keywordSearchService.getData('standardDivisions').subscribe(data => {
      data.forEach((item) => {
        this.stOptions.push(item['standard']['name']);
      });
      this.stList = data;
    });
  }

  // data get from orgoption
  getOrgOptions(): void {
    this.keywordSearchService.getData('organizations').subscribe(data => {
      data.forEach((item) => {
        this.orgOptions.push(item.name);
      });
    });
  }

  /**
  * option selection
  */
  onSelect(item, value) {
    this.stList.forEach((stItem) => {
      if (stItem['standard']['name'] === value) {
        item['id'] = stItem['division']['id'];
        item['division'] = stItem['division']['name'];
      }
    });
  }
}
