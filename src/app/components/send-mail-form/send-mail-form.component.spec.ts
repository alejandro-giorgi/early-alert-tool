import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { DatePickerComponent } from '../date-picker/date-picker.component';
import { TimePickerComponent } from '../time-picker/time-picker.component';
import { InputTagComponent } from '../input-tag/input-tag.component';
import { TagInputModule } from 'ngx-chips';
import { QuillModule } from 'ngx-quill';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { HttpClientModule } from '@angular/common/http';
import { SendMailFormComponent } from './send-mail-form.component';

describe('SendMailFormComponent', () => {
  let component: SendMailFormComponent;
  let fixture: ComponentFixture<SendMailFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMailFormComponent, TextboxComponent,
                      InputTagComponent, TimePickerComponent,
                      DatePickerComponent, DropdownComponent ],
      imports: [
        FormsModule,
        TagInputModule,
        QuillModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        HttpClientModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
