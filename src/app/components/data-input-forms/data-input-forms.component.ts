import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-data-input-forms',
  templateUrl: './data-input-forms.component.html',
  styleUrls: ['./data-input-forms.component.scss']
})
export class DataInputFormsComponent implements OnInit {
  @Input() activeTab: any;

  isEditedMode = false;
  parentStandardId = 0;
  standardId = 0;

  newCnSissue = {};
  productLines = [];
  emails = [];
  nameOfStandardMail = [];
  personsResponsibles = [];
  dataInputTab = true;
  newStandards = {standardParticipant: [], standard: {organization: {}}, division: {},
                 subDivistion: {}, productLine: [], nameOfStandardMail: [],
                 criticalToBusiness: false};

  standardOptions = [];
  divisionOptions = [];
  subDivisionOptions = [];
  regionOptions = [];

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  // sending request to fetch value
  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.isEditedMode = true;
        this.standardId = params['id'];

        this.dataService.getData('standardDivisions/' + params['id']).subscribe(data => {
          this.newStandards = data;

          if (this.newStandards.criticalToBusiness) {
            this.newStandards['criticalToBusinessShown'] = 'YES';
          } else {
            this.newStandards['criticalToBusinessShown'] = 'NO';
          }
          this.productLines = [];
          this.newStandards['productLine'].forEach((item) => {
            this.productLines.push(item.name);
          });

          this.emails = this.newStandards['standardParticipant'];
          this.nameOfStandardMail = this.newStandards['nameOfStandardMail'];
        });
      } else {
        this.isEditedMode = false;
        if (this.newStandards.criticalToBusiness) {
          this.newStandards['criticalToBusinessShown'] = 'YES';
        } else {
          this.newStandards['criticalToBusinessShown'] = 'NO';
        }
      }
    });

    this.newCnSissue = {personsResponsibles: []};
    this.dataService.getData('standards').subscribe(data => {
      data.forEach((item, index) => {
        if (!(Object.values(this.standardOptions).indexOf(item.name) > -1)) {
          this.standardOptions.push(item.name);
        }
      });
    });
    this.dataService.getData('divisions').subscribe(data => {
      data.forEach((item, index) => {
        if (!(Object.values(this.divisionOptions).indexOf(item.name) > -1)) {
          this.divisionOptions.push(item.name);
        }
        if (!(Object.values(this.regionOptions).indexOf(item.region) > -1)) {
          this.regionOptions.push(item.region);
        }
      });
    });

    this.dataService.getData('subDivisions').subscribe(data => {
      data.forEach((item, index) => {
        if (!(Object.values(this.subDivisionOptions).indexOf(item.name) > -1)) {
          this.subDivisionOptions.push(item.name);
        }
      });
    });
  }

  // tag value added in model
  onItemAdded(fieldName, event: any) {
    switch (fieldName) {
      case 'productLine':
        this.newStandards[fieldName].push({
          'id': this.newStandards[fieldName].length,
          'name': event.value
        });
        break;
      case 'standardParticipant':
      case 'nameOfStandardMail':
        this.newStandards[fieldName].push(event.value);
        break;
      case 'personsResponsibles':
        this.newCnSissue[fieldName].push(event.value);
        break;
      default:
        break;
    }
  }

  // tag value removed in model
  onItemRemoved(fieldName, event: any) {
    switch (fieldName) {
      case 'productLine':
        this.newStandards[fieldName].forEach((item, index) => {
          if (item['name'] === event) {
            this.newStandards[fieldName].splice(index, 1);
          }
        });
        break;
      case 'standardParticipant':
      case 'nameOfStandardMail':
        this.newStandards[fieldName].forEach((item, index) => {
          if (item === event) {
            this.newStandards[fieldName].splice(index, 1);
          }
        });
        break;
      case 'personsResponsibles':
        this.newCnSissue[fieldName].forEach((item, index) => {
          if (item === event) {
            this.newCnSissue[fieldName].splice(index, 1);
          }
        });
        break;
      default:
        break;
    }
  }

  // drop down value added in model
  onSelectCnS(prop: string, value: any) {
    this.newCnSissue[prop] = value;
  }

  // drop down value added in model
  onStandardSelect(prop: string, value: any) {
    const key = prop.split('.');
    if (key.length > 1) {
      this.newStandards[key[0]][key[1]] = value;
    } else {
      this.newStandards[key[0]] = value;
    }
  }

  // new standard submit
  onNewStandardSubmit() {
    if (this.newStandards['criticalToBusinessShown'] === 'YES') {
      this.newStandards.criticalToBusiness = true;
    } else {
      this.newStandards.criticalToBusiness = false;
    }

    if (!this.isEditedMode) {
      this.dataService.setData('standardDivisions', this.newStandards).subscribe(data => {
        this.router.navigate(['/keyword-search']);
      });
    } else {
      this.dataService.updateData('standardDivisions/' + this.standardId, this.newStandards).subscribe(data => {
        this.router.navigate(['/keyword-search']);
      });
    }
  }

  // onsubmit for new CnSissue
  onSubmit() {
    this.dataService.setData('CnSIssue', this.newCnSissue).subscribe(data => {
      this.router.navigate(['/send-email']);
    });
  }
}

