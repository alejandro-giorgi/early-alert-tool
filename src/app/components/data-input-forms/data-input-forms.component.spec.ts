import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TextboxComponent } from '../textbox/textbox.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { DatePickerComponent } from '../date-picker/date-picker.component';
import { TextareaComponent } from '../textarea/textarea.component';
import { InputTagComponent } from '../input-tag/input-tag.component';
import { TagInputModule } from 'ngx-chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { DataInputFormsComponent } from './data-input-forms.component';

describe('DataInputFormsComponent', () => {
  let component: DataInputFormsComponent;
  let fixture: ComponentFixture<DataInputFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataInputFormsComponent, TextboxComponent,
                      DatePickerComponent, DropdownComponent, InputTagComponent, TextareaComponent ],
      imports: [
        FormsModule,
        TagInputModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        HttpClientModule,
        RouterTestingModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataInputFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
