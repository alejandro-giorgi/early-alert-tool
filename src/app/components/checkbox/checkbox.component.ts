import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {

  @Input() label: string;
  @Input() isChecked: boolean;
  @Input() value: string;
  @Output() getChange = new EventEmitter();
  @Input() className: string;

  change() {
    this.getChange.emit(this.isChecked);
  }
}
