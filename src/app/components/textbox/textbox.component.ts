import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent {

  @Input() placeholder: string;
  @Input() type: string;
  @Input() name: string;
  @Input() className: string;
  @Input() isSearch: boolean;
  @Input() value: any;
  @Output() keyUp = new EventEmitter();

  /**
  * value send to the parent component
  * @param {any} event object
  */
  onKey(event: any) {
    this.keyUp.emit(this.value);
  }
}
