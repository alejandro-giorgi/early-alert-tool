import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentStandardsListComponent } from './recent-standards-list.component';

describe('RecentStandardsListComponent', () => {
  let component: RecentStandardsListComponent;
  let fixture: ComponentFixture<RecentStandardsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentStandardsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentStandardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
