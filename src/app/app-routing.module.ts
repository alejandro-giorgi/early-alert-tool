import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { KeywordSearchComponent } from './pages/keyword-search/keyword-search.component';
import { SendEmailComponent } from './pages/send-email/send-email.component';
import { DataInputComponent } from './pages/data-input/data-input.component';
import { StandardsOrgSearchComponent } from './pages/standards-org-search/standards-org-search.component';
import { EditStandardComponent } from './pages/edit-standard/edit-standard.component';
import { CSIssuesComponent } from './pages/c-s-issues/c-s-issues.component';
import { CSIssueDetailComponent } from './pages/c-s-issue-detail/c-s-issue-detail.component';
import { UserManagementComponent } from './pages/user-management/user-management.component';
import { ChangeRequestsComponent } from './pages/change-requests/change-requests.component';

/* Routing path */
const appRoutes: Routes = [
  { path: '', redirectTo: '/keyword-search', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'keyword-search', component: KeywordSearchComponent },
  { path: 'keyword-search/edit-standard/:id', component: EditStandardComponent },
  { path: 'standards-org-search', component: StandardsOrgSearchComponent },
  { path: 'send-email', component: SendEmailComponent },
  { path: 'data-input', component: DataInputComponent },
  { path: 'c-s-issues', component: CSIssuesComponent },
  { path: 'c-s-issues/detail/:id/:isTop', component: CSIssueDetailComponent },
  { path: 'change-requests', component: ChangeRequestsComponent },
  { path: 'user-management', component: UserManagementComponent },

  { path: '**', redirectTo: '/keyword-search' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
